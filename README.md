# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

This program simulates a simple web server that responds to .html and .css file in the pages directory.

The code in this program is originaly written by Prfessor Durairajan and modified by the author.

## Author: Yeaseul Shin, yshin@uoregon.edu ##
